import lxml.etree as ET

import numpy as np

#from geom import area

def pairs(lst):
    """Generaterator which returns all the sequential pairs of lst,
    treating lst as a circular list
    
    >>> list(pairs([1,2,3]))
    [(1, 2), (2, 3), (3, 1)]
    """     

    i = iter(lst)
    first = prev = i.next()
    for item in i:
        yield prev, item
        prev = item
    yield item, first

def area(poly):
    """ 
    Calculates the area of a polygon
    
    Args:
        poly: list of the vertices of a polygon in counterclockwise order

    Returns:
        float. Area of the polygon
    """
    return sum((0.5*(pp[0][0]*pp[1][1]-pp[1][0]*pp[0][1]) 
                for pp in pairs(poly)))




"""
Convert CellSeT xml files to the PLY format

CellSeT files are two dimensional: z-coordinate of vertices
are set to zero.
"""

flip_image = True

# Routine to convert xml attributes to strings or numbers,
# as appropriate
def to_val(s):
    s=str(s)
    if s=='None':
        return None
    else:
        try:
            s=int(s)
        except:
            try:
                s=float(s)
            except:
                pass
            pass
        return s


def unwind_cell_indices(index_pair_list):
    wall_set = set([tuple(pp) for pp in index_pair_list])
    wall = wall_set.pop()
    cell_idx = list(wall)
    end = wall[-1] 
    # Loop over all walls

    while wall_set:
        # While walls remain in wid_set
        for wall in wall_set:
            # Find wall in wid_set which contains the point end
            if end==wall[0]:
                # Add it to the ordered list of walls
                wall_set.discard(wall)
                # Set end to be the other end-point of the wall
                end = wall[-1]
                cell_idx.extend(wall[1:])
                break
            if end==wall[-1]:
                # Add it to the ordered list of walls
                wall_set.discard(wall)
                # Set end to be the other end-point of the wall
                end = wall[0]
                cell_idx.extend(wall[-2::-1])
                break
    return cell_idx
        
    

def load_xml(filename):

    tree=ET.parse(filename)
    root=tree.getroot()
    cells=tree.find('cells')

    cell_data = {}
    for c in cells.iter('cell'):
        walls=c.find('walls')
        cell_walls=[]
        for w in walls.iter('wall'):
            cell_walls.append(int(w.get('id')))
        cell_group=int(c.get('group'))
        old_cell_id=int(c.get('id'))
        truncated=(c.get('truncated') == 'True')
        cell_data[old_cell_id]=(cell_walls, cell_group, truncated)
  
    walls = tree.find('walls')
    wall_data = {}
    for w in walls.iter('wall'):
        pts = []
        for point in w.find('points').iter('point'):
            pts.append((float(point.get('x')), float(point.get('y'))))
        wall_data[int(w.get('id'))] = pts# [pts[0], pts[-1]]


    points = []
    pt_map = {}
    wall_points = {}
    for i, w in wall_data.iteritems():
        wall_idx = []
        for p in w:
            if p not in pt_map:
                pid = len(points)
                points.append(p)
                pt_map[p] = pid
            wall_idx.append(pt_map[p])
        wall_points[i] = wall_idx

#    print cell_data
#    print wall_data
#    print wall_points

    if flip_image:
        y_min = min(v[1] for v in points)
        y_max = max(v[1] for v in points)
        
        for i in range(len(points)):
            # pos[pid]=(v[0]*scale, (y_min+(y_max-v[1])*scale)      
            points[i] = (points[i][0], (y_min+(y_max-points[i][1])))

    cell_points = {}
    cell_types = {}
    for cid, (cell_walls, cell_group, truncated) in cell_data.iteritems():
        # Calculate the points on the boundary of each cell
        # Ensure that these are in counterclockwise orientation
        cell_wall_points = []
        for wid in cell_walls:
            cell_wall_points.append(wall_points[wid])
        print cell_wall_points
        ci = unwind_cell_indices(cell_wall_points)
        if area([points[i] for i in ci]):
            ci.reverse()
        cell_points[cid] = ci
        cell_types[cid] = cell_group
                   
    return points, cell_points, cell_types, wall_points

def write_ply(db, fn):
    """
    Write OpenAlea tissue file to ply format
    """
    points, cell_points, cell_types, wall_points = db
    f = open(fn, 'w')
    NV = len(points)
    NC = len(cell_points)
    NE = sum(len(w)-1 for w in wall_points.itervalues())
    header = "ply\nformat ascii 1.0\ncomment Geometry from CellSeT paper\n" + \
        "comment contact john.fozard@nottingham.ac.uk\n" + \
        "element vertex %d\n"%(NV) + \
        "property float x\nproperty float y\nproperty float z\n" + \
        "property uint index\n" + \
        "element face %d\n"%(NC) + \
        "property list uchar uint vertex_index\n" + \
        "property uint index\n"+ \
        "property list uchar float var\n"+ \
        "element edge %d\n"%(NE) + \
        "property uint source\n" + \
        "property uint target\n" + \
        "property uint index\n" + \
        "property list uchar float var\n" + \
        "end_header\n"
    f.write(header)
    for i in range(len(points)):
        f.write("%d %d 0 %d\n"%(points[i][0], points[i][1], i))
    for cid, cell in cell_points.iteritems():
        f.write(str(len(cell))+" "+" ".join(map(str, cell))+" "+str(cid)+" 1 "+str(cell_types[cid])+"\n")
    j = 0
    for wid, wall in wall_points.iteritems():
        for v0, v1 in zip(wall[:-1], wall[1:]):
            f.write(str(v0)+" "+str(v1)+" "+str(j)+" 1 "+str(wid)+"\n")
            j+=1
    f.close()

if __name__=='__main__':
    import sys
    db = load_xml(sys.argv[1])
    write_ply(db, sys.argv[2])

