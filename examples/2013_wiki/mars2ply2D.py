import os
import sys
import scipy.ndimage as nd
import numpy as np
from openalea.image.serial.basics import imread
import itertools

def distance(p1,p2):
    x1,y1,z1 = p1
    x2,y2,z2 = p2
    x=x2-x1
    y=y2-y1
    z=z2-z1
    return np.sqrt(x*x + y*y + z*z)

def scalaire(p1,p2):
    x1,y1,z1 = p1
    x2,y2,z2 = p2
    return x1*x2 + y1*y2 + z1*z2


def visualize(vertex2coordM, cell2bary):
    #here some basic visualization
    toto=np.array([j for j in vertex2coordM.values()])
    tata=np.array([j for j in cell2bary.values()])
    mlab.points3d(toto[:,0], toto[:,1],toto[:,2],color=(1,1,1),mode="2dvertex")
    mlab.points3d(tata[:,0], tata[:,1],tata[:,2],color=(1,0,1),mode="2dvertex")

def executeOrder66(source, target):
    print "reading mars file"
    m=imread(source)
    cell2bary={}
    print "extracting surface"
    m_un=m==1
    m_un_dil=nd.binary_dilation(m_un,iterations=2)-m_un
    matrice=np.array(m*m_un_dil)
    liste=list(np.unique(m*m_un_dil))
    if 0 in liste:liste.remove(0)
    if 1 in liste:liste.remove(1)
    ref=np.ones(m.shape)
    #ref[m*m_un_dil==0]=0
    bary=nd.center_of_mass(ref,matrice,liste)
    for i,j in enumerate(bary):
        cell2bary[liste[i]]=j

    print "calculating barycentres"
    cell2bary_complete={}
    liste=list(np.unique(m))
    bary_complete=nd.center_of_mass(ref,m,liste)
    for i,j in enumerate(bary_complete):
        cell2bary_complete[liste[i]]=j

    ################################## here see about the structuring element size
    print "calculating vertices"
    vertex2coord={}
    vertex2coord[0]={}
    vertex2coord[1]={}
    vertex2coord[2]={}
    vertex2coord[3]={}
    vertex2coord[4]={}
    vertex2coord[5]={}
    vertex2coord[6]={}
    #m_un=m==1
    #m_un_dil=nd.binary_dilation(m_un,iterations=2)-m_un
    liste=np.where(m*m_un_dil)
    m_deux=(m*m_un_dil).copy()
    tata=0
    for n,plouf in enumerate(liste[0]):
        if tata != int(n/float(len(liste[0])*10.)*100)*10:
            tata = int(n/float(len(liste[0])*10.)*100)*10
            print tata, "%"     
        i,j,k=liste[0][n], liste[1][n], liste[2][n]
        l=list(np.unique(m_deux[(i-1):(i+2),(j-1):(j+2),(k-1):(k+2)]))
        if 0 in l:l.remove(0)
        if 1 in l:l.remove(1)
        l=list(l)
        l.sort()
        vertex2coord[len(l)].setdefault(tuple(l),[]).append((i,j,k))

    print "filtering vertices"
    for rang in [6,5,4]:
        for n in [1,2,3]:
            for i in vertex2coord[rang].keys():
                for k in list(set([tuple(sorted(j)) for j in itertools.permutations(i,rang-n)])):
                    k=tuple(k)
                    if k in vertex2coord[rang-n].keys():
                        #print "filtre",rang,n,k, i
                        for el in vertex2coord[rang-n][k]:
                            vertex2coord[rang][i].append(el)
                        del vertex2coord[rang-n][k]

    #here i gather points belonging to the same vertex
    vertex2coordM={}
    for rang in [6,5,4,3]:
        for i in vertex2coord[rang].keys():
            if len(vertex2coord[rang][i])>0:
                vertex2coordM[i]=np.mean(np.array(vertex2coord[rang][i]),axis=0)

    print "renumbering vertices"
    #renumbering vertices
    k=0
    coord2id={}
    id2coord={}
    for i in sorted(vertex2coordM.keys()):
        coord2id[tuple(vertex2coordM[i])]=k
        id2coord[k]=tuple(vertex2coordM[i])
        k+=1

    for i in sorted(cell2bary.keys()):
        coord2id[cell2bary[i]]=k
        id2coord[k]=cell2bary[i]
        k+=1

    print "creating triangles"
    triangle=[]
    triangle_vectors={}
    cell_data=[]
    segments=[]
    segment_data=[]
    triangle_barycentre={}
    triangle_barycentre_complete={}
    for i in vertex2coordM.keys():
        a=set([tuple(sorted(list(toto))) for toto in itertools.permutations(i,2)])
        for j in vertex2coordM.keys():
            b=set([tuple(sorted(list(toto))) for toto in itertools.permutations(j,2)])
            if (i!=j) and (len(a.intersection(b))>0) :
                if len(a.intersection(b))>1:
                    #print "intersection should not be so huge", a,b, a.intersection(b)
                    pass
                for k in a.intersection(b):
                    #segment assigned to first triangle
                    vi,vj,vk=coord2id[tuple(vertex2coordM[i])], coord2id[tuple(vertex2coordM[j])], coord2id[tuple(cell2bary[k[0]])]
                    t=tuple([vi,vj,vk])
                    s=tuple([vi,vj])
                    if (t not in triangle) and ( ((t[1],t[0],t[2])) not in triangle) :
                        triangle.append(t)
                        cell_data.append(k[0])
                        triangle_barycentre[t]=(vertex2coordM[i] + vertex2coordM[j] + cell2bary[k[0]] )/3.
                        triangle_barycentre_complete[t]=cell2bary_complete[k[0]]
                        triangle_vectors[t] = [np.array(cell2bary[k[0]]) - vertex2coordM[i], np.array(cell2bary[k[0]]) - vertex2coordM[j]]
                    if s not in segments:
                        segments.append(s)
                        segment_data.append(distance(vertex2coordM[i], vertex2coordM[j]))
                    #segment assigned to second triangle
                    vi,vj,vk=coord2id[tuple(vertex2coordM[i])], coord2id[tuple(vertex2coordM[j])], coord2id[tuple(cell2bary[k[1]])]
                    t=tuple(([vi,vj,vk]))
                    s=tuple(([vi,vj]))
                    if t not in triangle and ( ((t[1],t[0],t[2])) not in triangle) :
                        triangle.append(t)
                        cell_data.append(k[1])
                        triangle_barycentre[t]=(vertex2coordM[i] + vertex2coordM[j] + cell2bary[k[1]] )/3.
                        triangle_barycentre_complete[t]=cell2bary_complete[k[1]]
                        triangle_vectors[t] = [np.array(cell2bary[k[1]]) - vertex2coordM[i], np.array(cell2bary[k[1]]) - vertex2coordM[j]]
                    if s not in segments:
                        segments.append(s)
                        segment_data.append(distance(vertex2coordM[i], vertex2coordM[j]))
    
    print "ordering triangles"
    triangle2=[]
    for t in triangle:
        norm=np.cross(triangle_vectors[t][0], triangle_vectors[t][1])
        link=triangle_barycentre[t]-triangle_barycentre_complete[t]
        s=scalaire(norm, link)
        triangle_provi=np.array(t)
        if s<0:
            triangle_provi[0], triangle_provi[1] = triangle_provi[1], triangle_provi[0]
        triangle2.append(tuple(triangle_provi))

    triangle=triangle2

    print "writing the file content"
    #header from meristem_correct.ply
    """
    ply
    format ascii 1.0
    comment Geometry (simplified) generated by tissue/organism
    comment http://www.thep.lu.se/~henrik/, henrik@thep.lu.se
    element vertex 3356
    property float x
    property float y
    property float z
    property uint index
    element face 1603
    property list uchar uint vertex_index
    property uint index
    property list uchar float var
    element edge 4958
    property uint source
    property uint target
    property uint index
    property list uchar float var
    end_header
    """
    nb_vertex=len(id2coord.keys())
    nb_faces=len(triangle)
    nb_edges=len(segments)
    header="""ply
    format ascii 1.0
    comment Geometry (simplified) generated by vincent Mirabet from MARS
    comment plop
    element vertex """+str(nb_vertex)+"""
    property float x
    property float y
    property float z
    property uint index
    element face """+str(nb_faces)+"""
    property list uchar uint vertex_index
    property uint index
    property list uchar float var
    element edge """+str(nb_edges)+"""
    property uint source
    property uint target
    property uint index
    property list uchar float var
    end_header
    """
    texte_vertex=""
    for i in sorted(id2coord.keys()):
        s=str(id2coord[i][0])+" "+str(id2coord[i][1])+" "+str(id2coord[i][2])
        s+=" "
        s+=str(i)
        s+="\n"
        texte_vertex+=s
    texte_faces=""
    for k,i in enumerate(sorted(triangle)):
        s=""
        s+=str(len(i))
        s+=" "
        s+=" ".join([str(ii) for ii in i])
        s+=" "
        s+=str(k)
        s+=" "
        s+="1 "
        s+=str(cell_data[k])
        s+="\n"
        texte_faces+=s

    texte_edges=""
    for k,i in enumerate(sorted(segments)):
        s=str(i[0])
        s+=" "
        s+=str(i[1])
        s+=" "
        s+=str(k)
        s+=" "
        s+="1 "
        s+=str(segment_data[k])
        s+="\n"
        texte_edges+=s

    print "writing the content on the disk"
    f=open(target,"w")
    f.write(header)
    f.write(texte_vertex)
    f.write(texte_faces)
    f.write(texte_edges)
    f.close()




def main():
    args = sys.argv[1:]
    if len(args)==0:
        print "\tright syntax : python source.inr.gz target.ply"
        sys.exit(0)
    if len(args)==2:
        if args[0].split(".")[-1] != "gz":
            print "\tWARNING you didn't specify a source with a gz format"
            sys.exit(0) 
        if args[1].split(".")[-1] != "ply":
            print "\tWARNING you didn't specify a target with ply extension"
            sys.exit(0) 
        print "lets convert", args[0], "into", args[1]
        executeOrder66(args[0], args[1])
        print "end of conversion"
    else:
        print "\tWARNING right syntax : python source.inr.gz target.ply"
        sys.exit(0)
    
if __name__ == '__main__':
    status = main()
    sys.exit(status)




















