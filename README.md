This sub group contains information on meshes for storing vertex-based descriptions of 
multicellular geometries.

It discusses:

Mesh formats:

The PlyFormat.tex holds information on the ply format as developed during the Sainsbury Workshop 2013. 

Graph format:

There has been a proposal to develop a PLY format with just vertices and Edges
- is it possible to view this in any viewer?
- do we want to indicate type in file name/extension? How do we tie graph/mesh files together?

Example files:

Example files to test for reading and writing can be found under examples.

More information can be found on [this subgroup's wiki](https://gitlab.com/morphodynamics/mesh/documentation/wikis/home).