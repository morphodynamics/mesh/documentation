%        File: PlyFormat.tex
%     Created: Tue Aug 28 04:00 pm 2012 B
% Last Change: Tue Aug 28 04:00 pm 2012 B
%
\documentclass[a4paper]{article}

\usepackage{vmargin}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{array}
\usepackage[pdftex,colorlinks=true,urlcolor=black,linkcolor=black,citecolor=black,pdfstartview=FitH,bookmarks=true]{hyperref}
\usepackage[utf8]{inputenc}

\setmarginsrb{25mm}{20mm}{25mm}{20mm}{0mm}{0mm}{10mm}{10mm}

\newcommand{\PLY}{PLY}

\begin{document}
\title{Exchange Format for Geometries}
\author{Pawel, Henrik, Gerardo, Alex, Richard and Pierre}

\maketitle

\section{Introduction}

The purpose of this document is to provide the basis for an exchange format for geometries so various modelling 
environment and image digitisation software can exchange shapes labelled with simulation/data analysis results.

The basic data structure to be exchanged are 2D and 3D cell complexes. The exact shape of each cell can either be left 
to the reader, or specified (e.g. simplicial subdivision). Other extensions will be agreed on when the need arises.

\section{Basics of the file format}

The file format is an extension of the \PLY\ file format, in which more elements are defined compared to the default.

\subsection{Basics of the \PLY\ file format.}

The \PLY\ file format is split in two parts: a human-readable header describing the content of the file, and the body, 
that contains the values given as ASCII or binary.

The order in which the elements are specified is unimportant and readers should be able to re-order the elements as 
suited.

\subsection{Header.}
The header has the following structure:

\begin{verbatim}
ply
format [FORMAT] [VERSION]
comment [COMMENT]
element [TYPE] [NUMBER]
property [TYPE] [NAME]
...
end_header
\end{verbatim}

The two first lines must correspond to the ones above. Comments may appear anywhere and should simply be ignored. The 
order in which the element appears defines their order of appearance in the body of the file. As for the properties, 
they are attached to the last element described, and their order define the order in correspond to their order in the 
body of the file.

The format must be one of \verb#ascii#, \verb#binary_little_endian# or \verb#binary_big_endian# and be followed by 
a version number. The present document will be using the version \verb#1.0#.

\begin{table}
  \centering
  \begin{tabular}{|c|l|c|}\hline
    \textbf{name} & \textbf{type} & \textbf{number of bytes}\\\hline
char   & character              & 1\\\hline
uchar  & unsigned character     & 1\\\hline
short  & short integer          & 2\\\hline
ushort & unsigned short integer & 2\\\hline
int    & integer                & 4\\\hline
uint   & unsigned integer       & 4\\\hline
float  & single-precision float & 4\\\hline
double & double-precision float & 8\\\hline
  \end{tabular}
  \caption{Valid property types}
  \label{tab:prop_types}
\end{table}

Properties can be either single values or lists. The types accepted are listed in table \ref{tab:prop_types}. In the 
case of lists, the syntax become:
\begin{verbatim}
property list [TYPE_SIZE] [TYPE_VALUE] [NAME]
\end{verbatim}
Where \verb#TYPE_SIZE# is the type used to give the number of values and \verb#TYPE_VALUE# the type of the values in the 
list.

\subsection{Standard elements.}
The standard defines 2 types of elements: vertices and faces. And they should appear in that order. The faces are 
defined by the ordered list of vertices defining the polygons. The minimum valid header for a polygon mesh is then:
\begin{verbatim}
ply
format [FORMAT] 1.0
element vertex [NB_VERTEX]
property float x
property float y
property float z
element face [NB_FACES]
property list uchar int vertex_index
end_header
\end{verbatim}

The full list of elements and their properties can be found in table
\ref{tab:list_elems}, \ref{tab:list_props} and \ref{tab:opt_props}.

\begin{table}
  \centering
  \begin{tabular}{|c|l|}\hline
    \textbf{Element} & \textbf{Description} \\\hline
    global & Global attributes \\\hline
    vertex & 0D cells \\\hline
    edge & 1D cells \\\hline
    face & 2D cells \\\hline
    volume & 3D cells \\\hline
  \end{tabular}
  \caption{List of available elements}
  \label{tab:list_elems}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{|c|c|c|p{7cm}|}\hline
    \textbf{Element} & \textbf{Property} & \textbf{Type} & \textbf{Description} \\\hline
    global & type & list of char & One of ``cell\_complex'', ``stratified\_space'',
    ``cell\_graph'', or other when needed\\\hline
    \multirow{3}{*}{vertex} & x & float & Position x \\
    & y & float & Position y \\
    & z & float & Position z \\\hline
    \multirow{2}{*}{edge} & source & uint & 0-base vertex id of the source of the edge \\
    & target & uint & 0-based vertex id of the target of the edge \\\hline
    face & vertex index & list uint & List of 0-based vertex ids defining the polygon \\\hline
    volume & face index & list int & List of signed 1-based face ids defining the face. The sign of the face id define 
    the relative orientation of the face w.r.t the volume \\\hline
  \end{tabular}
  \caption{List of mandatory properties per element.}
  \label{tab:list_props}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{|c|c|c|p{7cm}|}\hline
        \textbf{Element} & \textbf{Property} & \textbf{Type} & \textbf{Description} \\\hline
        \multirow{7}{*}{global} & oriented & uchar & 1 if the faces and edges are correctly oriented, 0 otherwise\\
        & multiple\_vertex & uchar & 0 if unique, or which dimension is the
        first that have separate vertices. e.g. if 2, edges shared vertices,
        but not faces\\
        & multiple\_edge & uchar & 0 if unique, or which dimension is the first
        that have separate vertices. e.g. if 2, faces do not share edges\\
        & multiple\_face & uchar & 0 if unique, 3 if not, as only volumes may not have the same faces\\
        & extra\_elements & uchar & 1 if there may be extra k-cells not part of the cell complex, 0 otherwise\\
        & associated\_file & list of char & Name of another file, to be read in association with this one (e.g. mesh of a cell graph or vice-versa)\\
        & length\_unit & float & Unit of lengths in the file in SI (e.g.\ 1 for $m$, $10^{-6}$ for $\mu m$, \ldots)\\\hline
        \multirow{4}{*}{vertex} & label & int & Unique cell identifier \\
        & signal & float & Generic signal intensity field \\
        & area & float & area of a cell (e.g.\ for cell graph) \\
        & volume & float & volume of a cell (e.g.\ for cell graph) \\\hline
        \multirow{3}{*}{edge} & x,y,z & float & position of the edge \\
        & length & float & length of the edge or interface (cell graph) \\
        & area & float & area of the interface (cell graph) \\\hline
        \multirow{3}{*}{face} & x,y,z & float & position of the face \\
        & label & int & List of 0-based vertex ids defining the polygon \\
        & area & float & List of 0-based vertex ids defining the polygon \\\hline
        \multirow{4}{*}{volume} & x,y,z & float & position of the volume \\
        & area & float & surface area of the cell \\
        & volume & float & volume of the cell \\
        & label & int & unique cell identifier (3D mesh) \\\hline
    \end{tabular}
    \caption{List of optional properties}\label{tab:opt_props}
\end{table}

\subsection{Subdivision elements.}
Any element of dimension 1 or greater, the cells might be subdivided, or at least their shape can be defined by other 
means. For this, we can add subdivision elements, that will specify the shapes. In general, the subdivided element is 
called \verb#subX#, where \verb#X# can be \verb#edge#, \verb#face# or \verb#volume#. Table \ref{tab:sub_prop} defines 
the properties suggested.

\begin{table}
  \centering
  \begin{tabular}{|c|c|l|}\hline
    \textbf{Name} & \textbf{Type} & \textbf{Description}\\\hline
    id & uint & 0-based id of the subdivided cell \\\hline
    type & uchar & Type of subdivision \\\hline
  \end{tabular}
  \caption{Properties for subdivided cells}
  \label{tab:sub_prop}
\end{table}

\end{document}
