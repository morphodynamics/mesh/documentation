\documentclass[a4paper]{article}

\usepackage{vmargin}
\usepackage{booktabs, longtable}
\usepackage{array}
\usepackage[pdftex,colorlinks=true,urlcolor=black,linkcolor=black,citecolor=black,pdfstartview=FitH,bookmarks=true]{hyperref}
\usepackage[utf8]{inputenc}

\setmarginsrb{25mm}{20mm}{25mm}{20mm}{0mm}{0mm}{10mm}{10mm}

\newcommand{\PLY}{PLY}

\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

\begin{document}
\title{Extended PLY\\Exchange Format for Geometries}
\author{2015: Pawel, Henrik, Gerardo, Alex, Richard and Pierre\\
             2017: Brendan, Guillaume, John, and Richard}
\date{September 1, 2017}

\maketitle

\section{Introduction}

The purpose of this document is to provide the basis for an exchange
format for geometries so various modelling environment and image
digitisation software can exchange shapes labelled with simulation/data
analysis results.

The basic data structure to be exchanged are 2D and 3D cell complexes.
The exact shape of each cell can either be left to the reader, or
specified (e.g.~simplicial subdivision). Other extensions will be agreed
on when the need arises.

\section{Basics of the file format}

The file format is an extension of the standard PLY~file format.
Quoting from the original spec:

\begin{quote}
Vertices and faces are two examples of ``elements'', and the bulk of a
PLY file is its list of elements. Each element in a given file has a
fixed number of ``properties'' that are specified for each element.
\end{quote}

A PLY~file consists of two parts: a human-readable header describing the
elements and their properties, followed by the data in ASCII or binary
format.

\subsection{The header}

The header has the following structure:

\begin{verbatim}
ply
format [FORMAT] [VERSION]
comment [COMMENT]
element [TYPE] [NUMBER]
property [TYPE] [NAME]
...
end_header
\end{verbatim}

The two first lines must be \texttt{ply} followed by \texttt{format}.
The format must be one of \texttt{ascii},
\texttt{binary\_little\_endian} or \texttt{binary\_big\_endian} and be
followed by a version number. The present document will be using version
\texttt{1.0}. Comments may appear on any other line of the header and
may simply be ignored.

Each element generally represents a geoemtric entity of a certain type:
vertices, edges, faces, cells, and so on. The order in which the
elements appear defines their order of appearance in the body of the
file. Properties are attached to the most recent element described, and
their order defines their order in the body of the file. Each property
can be either a single value or a list. The valid value types are listed
in the table:

\begin{longtable}[]{@{}llr@{}}
\toprule 
\textbf{name} & \textbf{type} & \textbf{number of bytes}\tabularnewline
\midrule
\endhead
\texttt{char} & byte & 1\tabularnewline
\texttt{uchar} & unsigned byte & 1\tabularnewline
\texttt{short} & short integer & 2\tabularnewline
\texttt{ushort} & unsigned short integer & 2\tabularnewline
\texttt{int} & integer & 4\tabularnewline
\texttt{uint} & unsigned integer & 4\tabularnewline
\texttt{float} & single-precision float & 4\tabularnewline
\texttt{double} & double-precision float & 8\tabularnewline
\bottomrule
\end{longtable}

The syntax for defining a single value is

\begin{verbatim}
property [TYPE] [NAME]
\end{verbatim}

while the syntax for defining a list value is

\begin{verbatim}
property list [TYPE_SIZE] [TYPE_VALUE] [NAME]
\end{verbatim}

Here \texttt{TYPE\_SIZE} is the type used to give the number of values
and \texttt{TYPE\_VALUE} the type of the values in the list. For maximal
compatibility, the \texttt{TYPE\_SIZE} should be an unsigned integral
type (generally \texttt{uchar} or \texttt{ushort}).

\subsection{Standard elements}

In order to maximize compatibility with existing PLY software, the two
default elements, \texttt{vertex} and \texttt{face}, should appear in
the file in that order. The first three properties of \texttt{vertex}
should be the vertex's \texttt{x}, \texttt{y}, and \texttt{z}
coordinates. The first property of \texttt{face} should be the ordered
list of vertex indices defining the polygons. The first vertex defined
in the PLY file has index 0, and indices increase from there. The
minimum valid header for a polygon mesh is then:

\begin{verbatim}
ply
format [FORMAT] 1.0
element vertex [NUM_VERTICES]
property float x
property float y
property float z
element face [NUM_FACES]
property list uchar uint vertex_index
end_header
\end{verbatim}

Other standard elements of the extended PLY format are:

\subsubsection{edge}

An edge has two default properties: the indices of its source and target
vertices.

\begin{verbatim}
element edge [NUM_EDGES]
property uint source 
property uint target
\end{verbatim}

\subsubsection{volume}

A volume has one default property: the list of \emph{signed} indices of
its bounding faces. \textbf{Note that this means there can be no face 0,
so the first face defined in the PLY file has index \emph{1}, and
indices increase from there.}

\begin{verbatim}
element volume [NUM_VOLUMES]
property list uchar int face_index
\end{verbatim}

\subsubsection{global}

Global properties, if any, are stored in a single \texttt{global}
element. There are no required global properties, but some possible
properties are described in the table.

\begin{verbatim}
element global 1
property uchar oriented
\end{verbatim}

\begin{longtable}[]{@{}rrl@{}}
\toprule
\begin{minipage}[b]{0.16\columnwidth}\raggedleft\strut
\textbf{Property}\strut
\end{minipage} & \begin{minipage}[b]{0.1\columnwidth}\raggedleft\strut
\textbf{Type}\strut
\end{minipage} & \begin{minipage}[b]{0.6\columnwidth}\raggedright\strut
\textbf{Description}\strut
\end{minipage}\tabularnewline
\midrule
\endhead
\begin{minipage}[t]{0.16\columnwidth}\raggedleft\strut
\texttt{oriented}\strut
\end{minipage} & \begin{minipage}[t]{0.1\columnwidth}\raggedleft\strut
\texttt{uchar}\strut
\end{minipage} & \begin{minipage}[t]{0.6\columnwidth}\raggedright\strut
1 if the faces and edges are correctly oriented, 0 otherwise.\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.16\columnwidth}\raggedleft\strut
\texttt{multiple\_face}\strut
\end{minipage} & \begin{minipage}[t]{0.1\columnwidth}\raggedleft\strut
\texttt{uchar}\strut
\end{minipage} & \begin{minipage}[t]{0.6\columnwidth}\raggedright\strut
0 if faces are unique, 3 if volumes do not share faces.\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.16\columnwidth}\raggedleft\strut
\texttt{multiple\_edge}\strut
\end{minipage} & \begin{minipage}[t]{0.1\columnwidth}\raggedleft\strut
\texttt{uchar}\strut
\end{minipage} & \begin{minipage}[t]{0.6\columnwidth}\raggedright\strut
0 if edges are unique, or the lowest dimension whose cells do not share
edges.\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.16\columnwidth}\raggedleft\strut
\texttt{multiple\_vertex}\strut
\end{minipage} & \begin{minipage}[t]{0.1\columnwidth}\raggedleft\strut
\texttt{uchar}\strut
\end{minipage} & \begin{minipage}[t]{0.6\columnwidth}\raggedright\strut
0 if vertices are unique; otherwise, the lowest dimension which does not
share vertices.\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.16\columnwidth}\raggedleft\strut
\texttt{length\_unit}\strut
\end{minipage} & \begin{minipage}[t]{0.1\columnwidth}\raggedleft\strut
\texttt{float}\strut
\end{minipage} & \begin{minipage}[t]{0.6\columnwidth}\raggedright\strut
SI unit of linear dimensions (e.g.~1 for meters, 10-6 for microns,
etc.).\strut
\end{minipage}\tabularnewline
\bottomrule
\end{longtable}

\subsection{Standard properties}

Some standard properties have been defined beyond the ones described in
the previous section.

\begin{itemize}
\tightlist
\item
  \texttt{x}, \texttt{y}, \texttt{z}: These can be used to specify the
  position of not only vertices, but any elements.
\item
  \texttt{nx}, \texttt{ny}, \texttt{nz}: These give the coordinates of a
  vector normal to the element.
\item
  \texttt{label}: An identifier for relating cells. (integral type)
\item
  \texttt{signal}: A generic signal intensity field. (floating-point
  type)
\item
  \texttt{length}, \texttt{area}, \texttt{volume}: These may be applied
  to entries of the appropriate dimension.
\item
  \texttt{measure}: May replace \texttt{length} in 1D elements,
  \texttt{area} in 2D elements, or \texttt{volume} in 3D elements.
\item
  \texttt{index}: A unique identifier corresponding to the element. This
  might be used to correlate elements in different PLY files, for
  example.
\end{itemize}

\subsubsection{Oriented properties}

A property might have two different values for an element, one for each
orientation. For instance, the PIN transporter polarizes independently
in each cell, so the concentration of PIN on a wall might differ in its
two adjacent cells. These can be stored as two properties, but to
indicate that the two properties are linked, their names should be the
same, with a \texttt{+} appended to the property of the element in
positive orientation and a \texttt{-} appended to the property of the
element in negative orientation. For example,

\begin{verbatim}
element face [NUM_FACES]
property list uchar uint vertex_index
property double PIN+
property double PIN-
\end{verbatim}

\subsection{Deletions from the 2015 documentation}

\begin{itemize}
\item
  Two global properties in the 2015 documentation (\texttt{type} and
  \texttt{associated\_file}) had a string type. Strings can be recorded
  as a list of characters, but this makes them much less human-readable
  in the ASCII file format. The
  reference implementation
  includes a \texttt{string} type, which reads in the characters between
  double quotes. However, the \texttt{string} type is not included in
  the original
  specification, so many PLY readers will not load a file that uses it.
  In light of these limitations, these global properties are no longer
  considered standard. If you choose to use a property of type
  \texttt{string}, be aware that many PLY-aware software packages will
  not be able to read these files.
\item
  The 2015 documentation included special elements for specifying the
  shape of subdivided elements. In absence of an existing use-case, they
  are no longer considered standard.
\end{itemize}
 
\end{document}